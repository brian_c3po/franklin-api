var sqlite3 = require('sqlite3').verbose()
var db

function initialize() {
  return new Promise((resolve,reject) => {
    var _db = new sqlite3.Database(':memory:', (err) => {
     if (err) {
       return console.error(err.message);
     }
     console.log('Connected to the in-memory SQlite database. Mocks');
     db = _db
     db.serialize(function () {
       db.run('CREATE TABLE users (id INTEGER PRIMARY KEY, name TEXT)')
       var stmt = db.prepare('INSERT INTO users (name) VALUES (?)')

       for (var i = 0; i < 10; i++) {
         stmt.run('Ipsum ' + i)
       }

       stmt.finalize(() => {
         resolve()
       })
     })

   });
  })
}

function getDb() {
  console.log(db);
  return db
}

module.exports = {
  initialize:initialize,
  getDb: getDb,
}
