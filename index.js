const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const port = 3002

app.get('/', (req, res) => res.send('Hello World!'))

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(bodyParser.urlencoded({ extended: false }))

app.use(bodyParser.json())

app.use('/api/v1', require('./routes'))

// setup generic error handler
app.use(errorHandler)

function errorHandler (err, req, res, next) {
  res.status(500)
  res.render('error', { error: err })
}

app.listen(port, () => console.log(`Example app listening on port ${port}!`))

module.exports = app;
