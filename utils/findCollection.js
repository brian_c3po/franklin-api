var db = require('../db.js')


module.exports = (tableName) => {
  return new Promise((resolve,reject) => {
    db.all(`SELECT * FROM ${tableName}`, {}, function(err,rows){
      if(err) reject(err)
      resolve(rows)
    })
  })
}
