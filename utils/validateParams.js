const models = require('../models')

module.exports = (tableName, params) => {
  let response = {valid: true};
  let columns = models[tableName];
  if(!columns){
    return response
  }
  for(let i = 0; i < columns.length; i ++){
    let column = columns[i]
    let columnName = column.column;
    let paramValue = params[columnName];
    if(!paramValue && column.required){
      response.message = `did not have the required parameter for this action: ${columnName}`
      response.valid = false
      break;
    }
    if(!typeCheck(paramValue, column.type)){
      response.valid = false
      response.message = `wrong value type, got '${typeof(paramValue)}' but need type '${column.type}' for parameter '${columnName}'`;
      break;
    }
  }
  return response;
}

function typeCheck(paramValue, type){
  let isValid = true;
  switch(type){
    case 'number':
      isValid = !isNaN(paramValue)
      break;
    default:
      isValid = typeof(paramValue) === type
  }
  return isValid
}
