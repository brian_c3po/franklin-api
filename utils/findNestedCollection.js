var db = require('../db.js')

module.exports = (tableName, foreignKey, foreignKeyValue) => {
  return new Promise((resolve,reject) => {
    db.all(`
      SELECT
        *
      FROM
        ${tableName}
      WHERE 
        ${foreignKey} = ${foreignKeyValue}`, {}, function(err,rows){
      if(err) reject(err)
      resolve(rows)
    })
  })
}
