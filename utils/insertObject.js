var db = require('../db.js')

module.exports = (tableName, columns, values) => {
  return new Promise((resolve,reject) => {
    db.run(`
      INSERT INTO
        ${tableName}
        (${columns})
      VALUES
        (${values})`, {}, function(err){
      if(err) reject(err)
      console.log(this)
      resolve({lastID:this.lastID})
    })
  })
}
