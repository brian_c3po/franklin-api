var findCollection = require('./findCollection')
var sqlite3 = require('sqlite3').verbose()

jest.mock('../db')
const db = require('../__mocks__/db.js')

beforeAll((done) => {
  db.initialize()
  .then(() => {
    done()
  })

});

describe('mocked memory db', () => {
  //console.log('testing mocked db');
  it('should have populated correctly', (done) => {
    //console.log('in test: ',db)
    _db = db.getDb()
    _db.all(`SELECT * FROM users`, {}, function(err,rows){
      expect(err).toBe(null)
      expect(rows.length).toBe(10)
      done()
    })
  })
})

/*
TODO: refactor the query utility functions to be testable

describe('get collection', () => {
  console.log('testing fetch utility functions ');
  it('should fetch all 10 mocked users', (done) => {
    findCollection('users')
      .then(rows => {
        expect(rows.length).toBe(10)
      })
  })
})
*/
