var validateParams = require('./validateParams')

let userNameParam = {
  name: function() {console.log('this is so wrong')}
}

let userParam2 = {
  name: 'valid'
}
// not valid
let userParam3 = {

}

describe('validate the parameters before attempting query',() => {
  it('should throw error if missing required field', () => {
    expect(validateParams('users', userParam3).valid).toBe(false)
  })

  it('should pass with a valid name', () => {
    expect(validateParams('users', userParam2).valid).toBe(true)
  })

  it('should fail if wrong datatype', () => {
    expect(validateParams('users', userNameParam).valid).toBe(false)
  })
})

describe('validation should work for tables with multiple columns', () => {
  it('should pass if params have all required columns', () => {
    expect(validateParams('order_items', {
      item_id: "123",
      order_id: 123
    }).valid).toBe(true)
  })

  it('should fail if params do not have all required columns', () => {
    expect(validateParams('order_items', {
      item_id: 123
    }).valid).toBe(false)
  })
})
