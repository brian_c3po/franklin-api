const routes = require('express').Router();
const users = require('express').Router();
const orders = require('express').Router();
const items = require('express').Router();
const order_items = require('express').Router();
const db = require('../db.js');
const findObject = require('../utils/findObject');
const findCollection = require('../utils/findCollection');
const findNestedCollection = require('../utils/findNestedCollection');
const insertObject = require('../utils/insertObject');
const validateParams = require('../utils/validateParams');

/**
GET /users
GET /users/:userId
GET /users/:userId/orders
POST /users
**/
users.get('/', (req,res) => {
  findCollection('users')
    .then(data => res.status(200).send(JSON.stringify(data)))
    .catch(err => res.status(500).send(JSON.stringify({code: err.code, message: err.message})))
})

users.get('/:userId', (req,res) => {
  const {userId} = req.params
  findObject('users', userId)
    .then(data => res.status(200).send(JSON.stringify(data)))
    .catch(err => res.status(500).send(JSON.stringify({code: err.code, message: err.message})))
})

users.get('/:userId/orders', (req, res) => {
  const {userId} = req.params
  findNestedCollection('orders', 'user_id', userId)
    .then(data => res.status(200).send(JSON.stringify(data)))
    .catch(err => res.status(500).send(JSON.stringify({code: err.code, message: err.message})))
})

users.post('/', (req,res) => {
  try{
    let inputValidation =  validateParams('users', req.body)
    if(!inputValidation.valid){
      res.status(422).send(JSON.stringify({
        valid: false,
        message: inputValidation.message
      }))
    } else {
      insertObject('users', 'name', `'${req.body.name}'`)
        .then(data => res.status(201).send(JSON.stringify(data)))
        .catch(err => res.status(500).send(JSON.stringify({code: err.code, message: err.message})))
    }
  } catch (err) {
    res.status(500).send(JSON.stringify({code: err.code, message: err.message}))
  }
})

/**
GET /orders
GET /orders/:orderId
**/
orders.get('/', (req,res) => {
  findCollection('orders')
    .then(data => res.status(200).send(JSON.stringify(data)))
    .catch(err => res.status(500).send(JSON.stringify({code: err.code, message: err.message})))
})

orders.get('/:orderId', (req,res) => {
  const {orderId} = req.params
  findObject('orders', orderId)
    .then(data => res.status(200).send(JSON.stringify(data)))
    .catch(err => res.status(500).send(JSON.stringify({code: err.code, message: err.message})))
})

orders.post('/', (req, res) => {
  try{
    let inputValidation =  validateParams('orders', req.body)
    if(!inputValidation.valid){
      res.status(422).send(JSON.stringify({
        valid: false,
        message: inputValidation.message
      }))
    } else {
      insertObject('orders', 'user_id', `'${req.body.user_id}'`)
        .then(data => res.status(201).send(JSON.stringify(data)))
        .catch(err => res.status(500).send(JSON.stringify({code: err.code, message: err.message})))
    }
  } catch (err) {
    res.status(500).send(JSON.stringify({code: err.code, message: err.message}))
  }
})

/**
GET   /items
GET   /items/:itemId
POST  /items
**/
items.get('/', (req,res) => {
  findCollection('items')
    .then(data => res.status(200).send(JSON.stringify(data)))
    .catch(err => res.status(500).send(JSON.stringify({code: err.code, message: err.message})))
})

items.get('/:itemId', (req,res) => {
  const {itemId} = req.params
  findObject('items', itemId)
    .then(data => res.status(200).send(JSON.stringify(data)))
    .catch(err => res.status(500).send(JSON.stringify({code: err.code, message: err.message})))
})

items.post('/', (req, res) => {
  try{
    let inputValidation =  validateParams('items', req.body)
    if(!inputValidation.valid){
      res.status(422).send(JSON.stringify({
        valid: false,
        message: inputValidation.message
      }))
    } else {
      insertObject('items', 'name', `'${req.body.name}'`)
        .then(data => res.status(201).send(JSON.stringify(data)))
        .catch(err => res.status(500).send(JSON.stringify({code: err.code, message: err.message})))
    }
  } catch (err) {
    res.status(500).send(JSON.stringify({code: err.code, message: err.message}))
  }
})

/**
GET /order-items
GET /order-items/:itemId
POST /order-items
**/
order_items.get('/', (req,res) => {
  findCollection('order_items')
    .then(data => res.status(200).send(JSON.stringify(data)))
    .catch(err => res.status(500).send(JSON.stringify({code: err.code, message: err.message})))
})

order_items.get('/:orderItemId', (req,res) => {
  const {orderItemId} = req.params
  findObject('order_items', orderItemId)
    .then(data => res.status(200).send(JSON.stringify(data)))
    .catch(err => res.status(500).send(JSON.stringify({code: err.code, message: err.message})))
})

order_items.post('/', (req, res) => {
  try{
    let inputValidation =  validateParams('order_items', req.body)
    if(!inputValidation.valid){
      res.status(422).send(JSON.stringify({
        valid: false,
        message: inputValidation.message
      }))
    } else {
      insertObject('order_items', 'order_id, item_id', `'${req.body.order_id}', '${req.body.item_id}'`)
        .then(data => res.status(201).send(JSON.stringify(data)))
        .catch(err => res.status(500).send(JSON.stringify({code: err.code, message: err.message})))
    }
  } catch (err) {
    res.status(500).send(JSON.stringify({code: err.code, message: err.message}))
  }
})


routes.use('/users', users)
routes.use('/orders', orders)
routes.use('/items', items)
routes.use('/order-items', order_items)

module.exports = routes;
