# How to run

assuming that you already have node and npm installed first clone the repo

If first time running `npm install` in the app directory to install the required dependencies

Run `npm start` to start the server

To run the test suite run `npm test`

while there is currently no GUI for crud actions. I recommend using a tool such as postman to interact with the API.

The api is setup to accept `x-www-form-urlencoded` content type for POST commands.

# API reference index

## users
| resource      | description                       |
|:--------------|:----------------------------------|
| `GET` /users                      | gets all users |
| `GET` /users/:userId              | gets a single user |
| `GET` /users/:userId/orders              | orders related to the user |
| `POST` /users                      | create a user |

Post Param:
- name (string)

## orders
| resource      | description                       |
|:--------------|:----------------------------------|
| `GET` /orders             | get all orders |
| `GET` /orders/:orderId    | get a single order |
| `POST` /orders            | create an order |

Post Param:
- user_id (number)

## items
| resource      | description                       |
|:--------------|:----------------------------------|
| `GET` /items            | get all items |
| `GET` /items/:itemId    | get a single item |
| `POST` /items           | create an item |

Post Param:
- name (string)

## order_items
| resource      | description                       |
|:--------------|:----------------------------------|
| `GET` /order-items                | get all order items |
| `GET` /order-items/:orderItemId   | get a single order item |
| `POST` /order-items                      | create an order item |

Post Params:

- order_id (number)

- item_id (number)


# Packages Used

- express

basic web framework for node, provides the tools to create an HTTP server

- sqlite3

library used to interface with sql lite database.

- jest

library for unit testing. provides the test framework and test runner.

- body-parser

middleware that parses the HTTP requests to simple objects.

# Next steps

- pagination
- filtering
- sanitize input
- add update and delete functionality
- refactor routes into individual files
