const sqlite3 = require('sqlite3').verbose()

var db = new sqlite3.Database('./db/franklin.db', (err) => {
  if (err) {
    return console.error(err.message);
  }
  console.log('Connected to the SQlite database.');
});


/*
db.serialize(function () {
  //db.run('CREATE TABLE users (id INTEGER PRIMARY KEY, name TEXT)')
  var stmt = db.prepare('INSERT INTO users (name) VALUES (?)')

  for (var i = 0; i < 10; i++) {
    stmt.run('Ipsum ' + i)
  }

  stmt.finalize()

  db.each('SELECT id, name FROM users', function (err, row) {
    console.log(row)
    console.log(row.id + ': ' + row.name)
  })
})
*/

//db.close()

module.exports = db
