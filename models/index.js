// models are used for input validation. 
module.exports = {
  users: [{
    column: 'name',
    type: 'string',
    required: true
  }],
  orders: [
    {
      column: 'user_id',
      type: 'number',
      required: true
    }
  ],
  items: [
    {
      column: 'name',
      type: 'string',
      required: true
    }
  ],
  order_items: [
    {
      column: 'order_id',
      type: 'number',
      required: true
    },
    {
      column: 'item_id',
      type: 'number',
      required: true
    }
  ]
}
